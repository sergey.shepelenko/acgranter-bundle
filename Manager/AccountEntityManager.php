<?php

namespace Acgranter\Bundle\Manager;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

use Acgranter\AccountUpdater\AccountEntityInterface;
use Acgranter\AccountUpdater\AccountUpdaterInterface;
use Acgranter\AccountUpdater\Exception\RuntimeException;
use Acgranter\AccountUpdater\AccountUpdaterFactory;

use Acgranter\AccountUpdater\AccountBag;


class AccountEntityManager
{
  const ACCOUNT_ACTION_ADD = 'add';
  const ACCOUNT_ACTION_UPDATE = 'update';
  const ACCOUNT_ACTION_REMOVE = 'remove';

  private $accountUpdaterFactory;
  private $doctrine;

  private $entityFileMap = [];

  private $accountUpdatersStore = ['password' => [], 'token' => [], 'ip' => []];

  private $acgranterReloadCmdArgs = [];

  /**
   * @param array $accountFiles
   */

  public function __construct(
    AccountUpdaterFactory $accountUpdaterFactory,
    ManagerRegistry $doctrine,
    array $entityFileMap = [],
    array $acgranterReloadCmdArgs = ['systemctl', 'reload', 'acgranter.service']
  )
  {
    $this->accountUpdaterFactory = $accountUpdaterFactory;
    $this->doctrine = $doctrine;
    $this->entityFileMap = $entityFileMap;
    $this->acgranterReloadCmdArgs = $acgranterReloadCmdArgs;
  }

  /**
   * @return array
   */
  public function getEntityFileMap(): array
  {
    return $this->entityFileMap;
  }

  /**
   * @param array $entityFileMap
   */
  public function setEntityFileMap(array $entityFileMap): void
  {
    $this->entityFileMap = $entityFileMap;
  }

  /**
   * @return array|string[]
   */
  public function getAcgranterReloadCmdArgs(): array
  {
    return $this->acgranterReloadCmdArgs;
  }

  /**
   * @param array|string[] $acgranterReloadCmdArgs
   */
  public function setAcgranterReloadCmdArgs(array $acgranterReloadCmdArgs): void
  {
    $this->acgranterReloadCmdArgs = $acgranterReloadCmdArgs;
  }

  /**
   *
   * @throws RuntimeException
   */
  public function syncAll($filter = []){
    $entityFileMap = $this->entityFileMap;
    if (count($filter) !== 0){
      if (isset($filter['entities'])){
        foreach ($filter['entities'] as $class){
          unset($entityFileMap[$class]);
        }
      }
      if (isset($filter['paths'])){
        foreach ($filter['paths'] as $path){
          foreach ($entityFileMap as $class => $fileMap){
            foreach ($fileMap as $type => $paths){
              $entityFileMap[$class][$type] = array_intersect($paths, [$path]);
            }
          }
        }
      }
    }
    foreach ($entityFileMap as $class => $fileMap){
      if (empty($fileMap) || !is_callable([$class, 'getActiveAccountCondition'])){
        continue;
      }
      $this->initAccountUpdaters($fileMap, false);

      $condition = $class::getActiveAccountCondition();
      $items = $this->doctrine->getRepository($class)->findBy($condition);
      foreach ($items as $item){
        /**
         * @var AccountEntityInterface $item
         */
        $accountBag = $item->getAccountBag();
        $this->applyAccountBag($fileMap, self::ACCOUNT_ACTION_ADD, $accountBag);
      }

      $this->mergeAndWrite();
    }
  }

  public function processAccountAction(string $class, string $action, AccountBag $accountBag){
    $fileMap = $this->entityFileMap[$class];

    $this->initAccountUpdaters($fileMap, true);

    $this->applyAccountBag($fileMap, $action, $accountBag);

    $this->mergeAndWrite();
  }

  public function reloadAcgranterDaemon(){
    if (count($this->acgranterReloadCmdArgs) === 0){
      return '';
    }
    $process = new Process($this->acgranterReloadCmdArgs);
    $process->run();
    $output = $process->getOutput();
    if (!$process->isSuccessful()) {
      throw new ProcessFailedException($process);
    }
    return trim($output);
  }

  protected function initAccountUpdaters($fileMap, $preloadContent = false){
    foreach ($fileMap as $type => $paths){
      foreach ($paths as $path){
        if (!isset($this->accountUpdatersStore[$type][$path])){
          $this->accountUpdatersStore[$type][$path] = $this->accountUpdaterFactory->getAccountUpdaterByFile($path);
          if ($preloadContent){
            $this->accountUpdatersStore[$type][$path]->readAccountsFromFile();
          }
        }
      }
    }
  }

  protected function applyAccountBag($fileMap, string $action, AccountBag $accountBag){
    $account = $accountBag->getAccount();
    foreach ($fileMap as $type => $paths){
      if (count($paths) === 0){
        continue;
      }
      switch ($action){
        case self::ACCOUNT_ACTION_ADD:
        case self::ACCOUNT_ACTION_UPDATE:
          $keysToSet = [];
          switch ($type){
            case 'password':
              if ($accountBag->isPasswordEnabled()){
                $password = $accountBag->getPassword();
                if ($password !== ''){
                  $keysToSet[] = $password;
                }
              }
              break;
            case 'token':
              if ($accountBag->isTokenEnabled()){
                $tokens = $accountBag->getTokens();
                if (count($tokens) !== 0){
                  $keysToSet = $tokens;
                }
              }
              break;
            case 'ip':
              if ($accountBag->isIpEnabled()){
                $ips = $accountBag->getIps();
                if (count($ips) !== 0){
                  $keysToSet = $ips;
                }
              }
              break;
          }
          if (count($keysToSet) !== 0){
            foreach ($paths as $path){
              $this->accountUpdatersStore[$type][$path]->setAccountKeys($account, $keysToSet);
            }
          }elseif ($action === self::ACCOUNT_ACTION_UPDATE){
            foreach ($paths as $path){
              $this->accountUpdatersStore[$type][$path]->removeAccount($account);
            }
          }
          break;
        case self::ACCOUNT_ACTION_REMOVE:
          foreach ($paths as $path){
            $this->accountUpdatersStore[$type][$path]->removeAccount($account);
          }
          break;
      }
    }
  }

  protected function mergeAndWrite(){
    foreach ($this->accountUpdatersStore as $accountUpdaters){
      foreach ($accountUpdaters as $accountUpdater){
        /**
         * @var $accountUpdater AccountUpdaterInterface
         */
        $accountUpdater->merge();
        $accountUpdater->writeAccountsToFile();
      }
    }
  }
}
