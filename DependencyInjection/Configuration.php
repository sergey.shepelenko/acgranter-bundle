<?php

namespace Acgranter\Bundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\NodeBuilder;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
  public function getConfigTreeBuilder()
  {
    $treeBuilder = new TreeBuilder('acgranter');
    $rootNode = $treeBuilder->getRootNode();

    $rootNode
      ->children()
      ->scalarNode('provider')->defaultNull()
      ->info("Optional. UserProvider used to load authenticated user.\n" .
        'When set to null the default UserProvider is used.')
      ->end()
      ->scalarNode('account_key')->defaultValue('REMOTE_USER')->end()
      ->scalarNode('status_key')->defaultNull()->end()
      ->scalarNode('status_value')->defaultNull()->end()
      ->scalarNode('acgranter_hash_cmd')->defaultValue('/usr/bin/acgranter-hash')->end()
      ->scalarNode('acgranter_reload_cmd')->defaultValue('systemctl reload acgranter.service')->end()
      ->end();

    $rootNode
      ->children()
        ->arrayNode('hashers')
        ->fixXmlConfig('hasher', 'hashers')
        ->info("Hashers that can be loaded from HasherFactory.\n" .
          "First argument is entity class or empty string. \n" .
          "Second argument is the name of hasher. Optional.")
          ->useAttributeAsKey('name')
          ->arrayPrototype()
            ->children()
              ->scalarNode('algorithm')->end()
              ->scalarNode('salt')->end()
              ->scalarNode('id')
                ->info('Custom hasher service ID')
              ->end()
              ->arrayNode('entities')
                ->fixXmlConfig('entity', 'entities')
                ->info('Restrict allowed entities.')
                  ->scalarPrototype()->end()
                ->end()
              ->end()
            ->end()
          ->end()
        ->end()
      ->end()
    ;

    $rootNode
      ->children()
        ->arrayNode('account_files')
          ->fixXmlConfig('account_file', 'account_files')
          ->info("Account files are CSV files used by acgranter daemon as Account Sources.\n" .
            "There are three types of files: password, token and ip.")
          ->useAttributeAsKey('path')
            ->arrayPrototype()
              ->children()
                ->enumNode('type')
                  ->values(['password', 'token', 'ip'])
                  ->defaultValue('password')
                ->end()
                ->scalarNode('account_updater')
                  ->info('Custom Account Updater service ID.')
                ->end()
                ->arrayNode('csv')
                  ->children()
                    ->scalarNode('separator')->defaultValue(",")->end()
                    ->scalarNode('enclosure')->defaultValue("\"")->end()
                    ->scalarNode('escape')->defaultValue("\\")->end()
                  ->end()
                ->end()
                ->arrayNode('filename_suffixes')
                  ->children()
                    ->scalarNode('old')
                      ->defaultValue('.old')
                      ->info("Suffix for saving old version of file. When set to empty string then old version is not saved.")
                    ->end()
                    ->scalarNode('new')
                      ->defaultValue('.new')
                      ->info("Suffix for new file for writing followed by renaming to target filename.\n" .
                        "When set to empty string then target file is rewritten directly.")
                    ->end()
                  ->end()
                ->end()
            ->end()
          ->end()
        ->end()
      ->end()
    ;

    $rootNode
      ->children()
        ->arrayNode('doctrine_account_entities')
        ->fixXmlConfig('doctrine_account_entity', 'doctrine_account_entities')
        ->info("Doctrine entities for catch those Persist/Update/Remove events\n" .
          "and update credentials information used by acgranter daemon.\n" .
          "Credentials information is saved in Credential files defined in this config.")
          ->useAttributeAsKey('entity')
          ->arrayPrototype()
            ->children()
              ->arrayNode('events')
                ->fixXmlConfig('event', 'events')
                ->info('Restrict used events: persist, update, remove.')
                  ->scalarPrototype()
                    ->validate()
                      ->ifNotInArray(['persist', 'update', 'remove'])
                      ->thenInvalid('Invalid doctrine event %s. Allowed values: persist, update, remove')
                    ->end()
                  ->end()
                ->end()
                ->arrayNode('account_files')
                  ->fixXmlConfig('account_file', 'account_files')
                  ->info('Restrict used account files.')
                  ->scalarPrototype()
                  ->end()
                ->end()
                ->arrayNode('types')
                  ->fixXmlConfig('type', 'types')
                  ->info('Restrict used account file types.')
                  ->scalarPrototype()
                    ->validate()
                    ->ifNotInArray(['password', 'token', 'ip'])
                    ->thenInvalid('Invalid account file type %s. Allowed values: password, token, ip')
                    ->end()
                  ->end()
                ->end()
              ->end()
            ->end()
          ->end()
        ->end()
      ->end();

    return $treeBuilder;
  }
}

