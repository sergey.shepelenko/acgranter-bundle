<?php

namespace Acgranter\Bundle\DependencyInjection;

use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;

use Acgranter\AccountUpdater\AccountEntityInterface;
use Acgranter\AccountUpdater\AccountUpdaterInterface;

class AcgranterExtension extends Extension
{

  public function load(array $configs, ContainerBuilder $container)
  {
    $configuration = $this->getConfiguration($configs, $container);
    $config = $this->processConfiguration($configuration, $configs);

    $loader = new XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
    $loader->load('services.xml');

    $definition = $container->getDefinition('acgranter.authenticator');
    if (null !== $config['provider']) {
      $definition->setArgument(0, new Reference($config['provider']));
    }
    if (null !== $config['account_key']) {
      $definition->setArgument(1, $config['account_key']);
    }
    if (null !== $config['status_key']) {
      $definition->setArgument(2, $config['status_key']);
    }
    if (null !== $config['status_value']) {
      $definition->setArgument(3, $config['status_value']);
    }

    $filesMap = ['password' => [], 'token' => [], 'ip' => []];
    if (count($config['account_files']) != 0) {
      foreach ($config['account_files'] as $path => $item){
        $type = $item['type'];
        $filesMap[$type][] = $path;
      }

      $definition = $container->getDefinition('acgranter.account_updater_factory');
      $definition->setArgument(0, $config['account_files']);
      $definition->setArgument(1, $config['doctrine_account_entities']);
    }

    if (count($config['doctrine_account_entities']) !== 0){
      $entityFileMap = [];
      $doctrineEntityListenerDefinition = $container->getDefinition('acgranter.doctrine_entity_listener');
      foreach ($config['doctrine_account_entities'] as $class => $item){
        $interface = AccountEntityInterface::class;
        if (!is_a($class, $interface, true)) {
          throw new InvalidConfigurationException(sprintf('Entity class "%s" must implement "%s".', $class, $interface));
        }
        $entityFileMapItem = [];
        if (count($item['account_files']) !== 0){
          $unknownAccountFiles = array_diff($item['account_files'], array_keys($config['account_files']));
          if (count($unknownAccountFiles) !== 0){
            throw new InvalidConfigurationException(sprintf('Account file(s) "%s" not configured.', implode('", "', $unknownAccountFiles)));
          }
        }
        if (count($item['types']) !== 0){
          foreach ($item['types'] as $type){
            if (count($item['account_files']) !== 0){
              $entityFileMapItem[$type] = array_intersect($filesMap[$type], $item['account_files']);
            }else{
              $entityFileMapItem[$type] = $filesMap[$type];
            }
          }
        }else{
          foreach ($filesMap as $type => $files){
            if (count($item['account_files']) !== 0){
              $entityFileMapItem[$type] = array_intersect($filesMap[$type], $item['account_files']);
            }else{
              $entityFileMapItem[$type] = $files;
            }
          }
        }
        $entityFileMap[$class] = $entityFileMapItem;

        $events = count($item['events']) !== 0 ? $item['events'] : ['persist', 'update', 'remove'];
        foreach ($events as $event){
          switch ($event){
            case 'persist':
              $doctrineEntityListenerDefinition->addTag('doctrine.orm.entity_listener', [
                'event'          => 'postPersist',
                'entity'         => $class
              ]);
              break;
            case 'update':
              $doctrineEntityListenerDefinition->addTag('doctrine.orm.entity_listener', [
                'event'          => 'postUpdate',
                'entity'         => $class
              ]);
              break;
            case 'remove':
              $doctrineEntityListenerDefinition->addTag('doctrine.orm.entity_listener', [
                'event'          => 'postRemove',
                'entity'         => $class
              ]);
              break;
          }
        }
      }

      $definition = $container->getDefinition('acgranter.account_entity_manager');
      $definition->setArgument(2, $entityFileMap);
      if ('' !== (string) $config['acgranter_reload_cmd']) {
        $cmdArg = explode(' ', $config['acgranter_reload_cmd']);
        $definition->setArgument(3, $cmdArg);
      }else{
        $definition->setArgument(3, []);
      }
    }

    $hashers = [];
    if (!empty($config['hashers'])){
      foreach ($config['hashers'] as $name => $item){
        if (isset($item['id'])){
          $item['id'] = new Reference($item['id']);
        }
        $hashers[$name] = $item;
      }
    }

    $definition = $container->getDefinition('acgranter.hasher_factory');
    $definition->setArgument(0, $hashers);
    $definition->setArgument(1, (string) $config['acgranter_hash_cmd']);

  }
}
