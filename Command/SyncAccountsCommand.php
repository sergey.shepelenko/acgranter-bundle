<?php

namespace Acgranter\Bundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

use Acgranter\Bundle\Manager\AccountEntityManager;

class SyncAccountsCommand extends Command
{
  protected static $defaultName = 'acgranter:sync-accounts';

  private $accountEntityManager;

  /**
   * @param AccountEntityManager $accountEntityManager
   */
  public function __construct(AccountEntityManager $accountEntityManager)
  {
    parent::__construct();
    $this->accountEntityManager = $accountEntityManager;
  }

  protected function configure(): void
  {
    $this
      ->setDescription('Sync account names and credentials from symfony entities to the account source CSV files.')
      ->addArgument('path', InputArgument::OPTIONAL, 'File to update', '');
  }

  protected function execute(InputInterface $input, OutputInterface $output): int
  {
    $path = $input->getArgument('path');
    $filter = !empty($path) ? ['paths' => [$path]] : [];
    $this->accountEntityManager->syncAll($filter);

    $cmdOutput = $this->accountEntityManager->reloadAcgranterDaemon();

    if (!empty($cmdOutput)){
      $output->writeln($cmdOutput);
    }

    $output->writeln('OK.');
    return Command::SUCCESS;
  }
}
