AcgranterBundle
==============

Install bundle as usual:
```bash
composer install acgranter/acgranter-bundle
```

Create initial config file:

```bash
php bin/console config:dump-reference acgranter > ./config/packages/acgranter.yaml
```

Edit config file according to your preferences. Refer to the Acgranter documentation:
https://gitlab.com/sergey.shepelenko/acgranter
