<?php

namespace Acgranter\Bundle\EventListener;

use Acgranter\AccountUpdater\AccountEntityInterface;
use Acgranter\Bundle\Manager\AccountEntityManager;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Psr\Log\LoggerInterface;

class DoctrineEntityListener
{
  private $accountEntityManager;
  private $logger;

  private $accountUpdatersStore = ['password' => [], 'token' => [], 'ip' => []];

  /**
   * @param AccountEntityManager $accountEntityManager
   */
  public function __construct(AccountEntityManager $accountEntityManager, LoggerInterface $logger = null)
  {
    $this->accountEntityManager = $accountEntityManager;
    $this->logger = $logger;
  }

  protected function reloadAcgranterDaemon(){
    $cmdOutput = $this->accountEntityManager->reloadAcgranterDaemon();
    if (!empty($cmdOutput)){
      $this->logger->info("reloading acgranter daemon", [
        'output' => $cmdOutput
      ]);
    }
  }

  public function postPersist(AccountEntityInterface $entity, LifecycleEventArgs $event): void
  {
    $accountBag = $entity->getAccountBag();
    $this->accountEntityManager->processAccountAction(get_class($entity), AccountEntityManager::ACCOUNT_ACTION_ADD, $accountBag);
    $this->reloadAcgranterDaemon();
  }

  public function postUpdate(AccountEntityInterface $entity, LifecycleEventArgs $event): void
  {
    $accountBag = $entity->getAccountBag();
    $this->accountEntityManager->processAccountAction(get_class($entity), AccountEntityManager::ACCOUNT_ACTION_UPDATE, $accountBag);
    $this->reloadAcgranterDaemon();
  }

  public function postRemove(AccountEntityInterface $entity, LifecycleEventArgs $event): void
  {
    $accountBag = $entity->getAccountBag();
    $this->accountEntityManager->processAccountAction(get_class($entity), AccountEntityManager::ACCOUNT_ACTION_REMOVE, $accountBag);
    $this->reloadAcgranterDaemon();
  }
}

